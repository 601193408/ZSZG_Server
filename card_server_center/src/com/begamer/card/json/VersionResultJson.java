package com.begamer.card.json;

public class VersionResultJson
{
	//==0版本不符,1版本ok==//
	public int mark;
	public String binPath;//下载bin文件路径,或下载游戏包路径//

	public int getMark()
	{
		return mark;
	}

	public void setMark(int mark)
	{
		this.mark = mark;
	}

	public String getBinPath()
	{
		return binPath;
	}

	public void setBinPath(String binPath)
	{
		this.binPath = binPath;
	}
	
}
