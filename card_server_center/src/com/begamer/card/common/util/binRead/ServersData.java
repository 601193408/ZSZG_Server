package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.List;

public class ServersData implements PropertyReader {
	
	public int id;
	public String name;
	public String ip;
	public int port;
	public int type;// 0推荐,1新服,2其他,3默认
	public int state;// 1维护,2爆满

	private static List<ServersData> data = new ArrayList<ServersData>();

	@Override
	public void addData() {
		data.add(this);
	}

	@Override
	public void parse(String[] ss) {
	}

	@Override
	public void resetData() {
		data.clear();
	}

	public static List<ServersData> getDatas() {
		return data;
	}

	public static boolean isValidIp(String ip) {
		if ("127.0.0.1".equals(ip)) {
			return true;
		}
		for (ServersData sd : data) {
			if (sd.ip.equals(ip)) {
				return true;
			}
		}
		return false;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

}
