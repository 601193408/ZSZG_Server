package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.begamer.card.common.util.StringUtil;
import com.begamer.card.json.CardJson;
import com.begamer.card.log.ErrorLogger;

public class FBeventData implements PropertyReader
{
	/**编号**/
	public int id;
	/**关卡名称**/
	public String name;
	/**清空类型 1每日清空，2按活动结束时间清空**/
	public int timestyle;
	/**场景**/
	public String scene;
	/**类型**/
	public int type;
	/**副本关卡类型  1.死亡洞窟,2.其他关卡**/
	public int leveltype;
	public String atlas;//图集
	/**boss头像**/
	public String bossicon;
	/**进入次数**/
	public int entry;
	/**进入等级**/
	public int unlocklevel;
	/**消耗体力**/
	public int cost;
	/**怪的数据**/
	public String[] monsters;
	/**BOSS技能和cd**/
	public String[] bossSkills;
	/**人物经验**/
	public int personexp;
	/**卡牌经验**/
	public int cardexp;
	/**掉落金币**/
	public int coins;
	/**掉落物品**/
	public List<String> drops;
	
	private static HashMap<Integer, FBeventData> data =new HashMap<Integer, FBeventData>();
	private static final Logger errorlogger=ErrorLogger.logger;
	@Override
	public void addData()
	{
		data.put(id, this);
	}

	@Override
	public void parse(String[] ss)
	{
		int location =0;
		id =StringUtil.getInt(ss[location]);
		name =StringUtil.getString(ss[location+1]);
		timestyle =StringUtil.getInt(ss[location+2]);
		scene =StringUtil.getString(ss[location+3]);
		type =StringUtil.getInt(ss[location+4]);
		leveltype = StringUtil.getInt(ss[location+5]);
		atlas =StringUtil.getString(ss[location+6]);
		bossicon =StringUtil.getString(ss[location+7]);
		entry =StringUtil.getInt(ss[location+8]);
		unlocklevel =StringUtil.getInt(ss[location+9]);
		cost =StringUtil.getInt(ss[location+10]);
		monsters =new String [6];
		for(int i=0;i<6;i++)
		{
			location=11+i*7;
			int monster =StringUtil.getInt(ss[location]);
			int level =StringUtil.getInt(ss[location+1]);
			int atk =StringUtil.getInt(ss[location+2]);
			int def =StringUtil.getInt(ss[location+3]);
			int hp =StringUtil.getInt(ss[location+4]);
			int skill =StringUtil.getInt(ss[location+5]);
			int boss =StringUtil.getInt(ss[location+6]);
			String monter_info =monster+"-"+level+"-"+atk+"-"+def+"-"+hp+"-"+skill+"-"+boss;
			monsters[i] =monter_info;
		}
		bossSkills =new String[3];
		for(int i=0;i<3;i++)
		{
			location=11+7*6+i*2;
			int bossSkill=StringUtil.getInt(ss[location]);
			int cd=StringUtil.getInt(ss[location+1]);
			bossSkills[i]=bossSkill+"-"+cd;
		}
		location =11+7*6+2*3;
		personexp =StringUtil.getInt(ss[location]);
		cardexp =StringUtil.getInt(ss[location+1]);
		coins =StringUtil.getInt(ss[location+2]);
		drops=new ArrayList<String>();
		for(int i=0;i<6;i++)
		{
			location=11+7*6+3*2+3+i*3;
			int drop=StringUtil.getInt(ss[location]);
			String pro=StringUtil.getString(ss[location+1]);
			int ability = StringUtil.getInt(ss[location+2]);
			try
			{
				if(!"".equals(pro) && ability>0)
				{
					if(drop ==1 || drop ==3 ||drop ==6)
					{
						String [] temp =pro.split(",");
						int dropId =StringUtil.getInt(temp[0]);
						int num =StringUtil.getInt(temp[1]);
					}
					drops.add(drop+"-"+pro+"-"+ability);
				}
			}
			catch (Exception e)
			{
				errorlogger.error("加载fbevent表出错,掉落数据格式错误"+id,e);
				System.exit(0);
			}
			
			if(!"".equals(pro))
			{
				drops.add(drop+"-"+pro+"-"+ability);
			}
		}
		addData();
	}

	@Override
	public void resetData()
	{
		data.clear();
	}
	
	/**根据id获取fbeventdata**/
	public static FBeventData getFBeventData(int index)
	{
		return data.get(index);
	}
	
	public static CardJson[] getMonsterData(int index)
	{
		FBeventData fbd =getFBeventData(index);
		if(fbd==null)
		{
			return null;
		}
		CardJson[] cjs=new CardJson[6];
		
		for(int i=0;i<6;i++)
		{
			String[] ss=fbd.monsters[i].split("-");
			int cardId=StringUtil.getInt(ss[0]);
			int level=StringUtil.getInt(ss[1]);
			int atk=StringUtil.getInt(ss[2]);
			int def=StringUtil.getInt(ss[3]);
			int maxHp=StringUtil.getInt(ss[4]);
			int skillId=StringUtil.getInt(ss[5]);
			int bossMark=StringUtil.getInt(ss[6]);
			
			int criRate=0;
			int aviRate=0;
			int talent1=0;
			int talent2=0;
			int talent3=0;
			
			if(CardData.getData(cardId)==null)
			{
				continue;
			}
			cjs[i]=new CardJson(i, cardId, level, skillId, atk, def, maxHp, bossMark, criRate, aviRate, talent1, talent2, talent3);
		}
		return cjs;
	}
}
