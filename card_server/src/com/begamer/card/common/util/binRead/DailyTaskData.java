package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.begamer.card.common.util.StringUtil;
import com.begamer.card.log.ErrorLogger;

public class DailyTaskData implements PropertyReader {
	public int id;
	public String name;
	public int type;
	public String description;
	public int request;

	public int unlockcondition;
	public String unlockdescription;

	public String icon;

	public int num;

	public List<String> reward;

	private static HashMap<Integer, DailyTaskData> data = new HashMap<Integer, DailyTaskData>();
	private static List<DailyTaskData> dataList = new ArrayList<DailyTaskData>();
	private static final Logger errorlogger = ErrorLogger.logger;

	@Override
	public void addData() {
		data.put(id, this);
		dataList.add(this);
	}

	@Override
	public void parse(String[] ss) {
		int location = 0;
		id = StringUtil.getInt(ss[location]);
		name = StringUtil.getString(ss[location + 1]);
		type = StringUtil.getInt(ss[location + 2]);
		description = StringUtil.getString(ss[location + 3]);
		request = StringUtil.getInt(ss[location + 4]);
		unlockcondition = StringUtil.getInt(ss[location + 5]);
		unlockdescription = StringUtil.getString(ss[location + 6]);
		icon = StringUtil.getString(ss[location + 7]);
		num = StringUtil.getInt(ss[location + 8]);
		reward = new ArrayList<String>();
		for (int i = 0; i < 3; i++) {
			location = 9 + i * 2;
			int rewardType = StringUtil.getInt(ss[location]);
			String rewardid = StringUtil.getString(ss[location + 1]);
			String str = rewardType + "-" + rewardid;
			try {
				if (rewardType == 0) {
					continue;
				} else {
					if (rewardType <= 5) {
						String[] temp = rewardid.split(",");
						int ward = StringUtil.getInt(temp[0]);
						int num = StringUtil.getInt(temp[1]);
					}
					reward.add(str);
				}
			} catch (Exception e) {
				errorlogger.error("加载dailytask奖励数据格式错误:" + id, e);
				System.exit(0);
			}
		}
		addData();
	}

	@Override
	public void resetData() {
		data.clear();
		dataList.clear();
	}

	/** 根据id获取data **/
	public static DailyTaskData getDailyTaskData(int index) {
		return data.get(index);
	}

	/** 所有data **/
	public static List<DailyTaskData> getDailyTaskDatas() {
		return dataList;
	}

	public static Integer getdataSize() {
		return dataList.size();
	}

}
