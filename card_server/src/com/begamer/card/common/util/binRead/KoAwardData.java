package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.begamer.card.common.util.StringUtil;
import com.begamer.card.log.ErrorLogger;
public class KoAwardData implements PropertyReader
{
	public int id;//id
	public int number;//ko个数
	public int numName;//组名称 
	public int numNum;//组名
	public String discretion;//描述
	public int rewardtyp1;
	public String reward1;
	private static HashMap<Integer, KoAwardData>  data =new HashMap<Integer, KoAwardData>();
	private static List<KoAwardData> dataList =new ArrayList<KoAwardData>();
	private static final Logger errorlogger=ErrorLogger.logger;
	@Override
	public void addData()
	{
		try 
		{
			if(rewardtyp1<=5)
			{
				String [] temp =reward1.split(",");
				int rewardId =StringUtil.getInt(temp[0]);
				int num =StringUtil.getInt(temp[1]);
			}
			data.put(id, this);
			dataList.add(this);
		} 
		catch (Exception e) 
		{
			errorlogger.error("加载koaward表奖励物品出错:"+id,e);
			System.exit(0);
		}
		
	}

	@Override
	public void parse(String[] ss)
	{
	}

	@Override
	public void resetData()
	{
		dataList.clear();
		data.clear();
	}

	/**根据id获取一个data**/
	public static KoAwardData getKoAwardData(int index)
	{
		return data.get(index);
	}
	
	/**获取同一name的datas**/
	public static List<KoAwardData> getAwardDatasByName(int name)
	{
		List<KoAwardData> list =new ArrayList<KoAwardData>();
		for(KoAwardData kd:data.values())
		{
			if(kd.numName ==name)
			{
				list.add(kd);
			}
		}
		return list;
	}
	
	public static List<KoAwardData> getAwardDatas()
	{
		return dataList;
	}
}
