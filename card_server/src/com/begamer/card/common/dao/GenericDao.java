package com.begamer.card.common.dao;

import java.io.Serializable;

import java.util.List;
import java.util.Set;
import java.util.Map.Entry;

import org.hibernate.Session;

public interface GenericDao<T, PK extends Serializable> {
	PK create(T newInstance);

	PK hibCreate(T newInstance);

	T read(PK id);

	void update(T transientObject);

	void hibUpdate(T transientObject);

	void updateWithoutLog(T transientObject);

	void delete(T persistentObject);

	List<T> load(int index, int count, String where, String orderby);

	List<Integer> loadByGroup(int index, int count, String where, String orderby);

	List<T> loadWithParameterType(int index, int count, String where,
			String orderby, List<HqlParameter> parameters);

	Long count(String where);

	Long countGroup(String where);

	Long countSql(String sql);

	Long countSqlPure(String sql);

	Long countHqlPure(String hql);

	Long countRecommend(String where);

	List<T> loadSql(int index, int count, String sql);

	List<T> loadByProperty(String propertyName, Object value);

	List<T> loadHqlPure(final int index, final int count, String hql);

	int executeUpdate(String hql);

	int executeSqlUpdate(String sql);

	int executeUpdateProperties(Set<Entry<String, String>> propSet, String where);

	public Session HibSession();
	
	public List<T> loadByHql(String hql);
}
