package com.begamer.card.cache;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.log4j.Logger;

import com.begamer.card.log.ErrorLogger;
import com.begamer.card.log.MemLogger;
import com.begamer.card.model.pojo.LogBuy;
import com.begamer.card.model.pojo.Mail;
import com.begamer.card.model.pojo.PkRank;
import com.begamer.card.model.pojo.Player;
import com.begamer.card.model.pojo.SpeciaMail;
import com.begamer.card.model.pojo.TimeMail;

public class MailThread implements Runnable {
	
	private static final Logger errorlogger = ErrorLogger.logger;
	private static final Logger memlogger = MemLogger.logger;
	
	private static MailThread instance;
	private static boolean running = true;
	private CopyOnWriteArrayList<TimeMail> timeMails = new CopyOnWriteArrayList<TimeMail>();
	private static final long PrintLogTime = 1 * 3600 * 1000;
	private static long lastPrintLogTime;
	private CopyOnWriteArrayList<SpeciaMail> speciaMails = new CopyOnWriteArrayList<SpeciaMail>();
	private List<LogBuy> logBuys = new ArrayList<LogBuy>();
	private Object lock = new Object();
	
	public static MailThread getInstance()
	{
		if (instance == null)
		{
			instance = new MailThread();
			new Thread(instance).start();
			memlogger.info("启动定时发送邮件线程");
		}
		return instance;
	}
	
	private MailThread()
	{
	}
	
	public boolean addTimeMail(TimeMail tm)
	{
		if (tm == null)
		{
			return false;
		}
		timeMails.add(tm);
		return true;
	}
	
	public boolean delTimeMail(TimeMail tm)
	{
		if (tm == null)
		{
			return false;
		}
		for (TimeMail timeMail : timeMails)
		{
			if (timeMail.getId() == tm.getId())
			{
				timeMails.remove(timeMail);
				break;
			}
		}
		return true;
	}
	
	public boolean addSpeciaMail(SpeciaMail sm)
	{
		if (sm == null)
		{
			return false;
		}
		speciaMails.add(sm);
		return true;
	}
	
	public boolean delSpeciaMail(SpeciaMail sm)
	{
		if (sm == null)
		{
			return false;
		}
		for (SpeciaMail speciaMail : speciaMails)
		{
			if (speciaMail.getId() == sm.getId())
			{
				timeMails.remove(speciaMail);
				break;
			}
		}
		return true;
	}
	
	/** 添加购买日志 **/
	public void addLogbuy(LogBuy lb)
	{
		synchronized (lock)
		{
			logBuys.add(lb);
		}
	}
	
	@Override
	public void run()
	{
		while (running)
		{
			try
			{
				long curTime = System.currentTimeMillis();
				// 定时邮件
				for (TimeMail tm : timeMails)
				{
					int validNum = tm.valid(curTime);
					if (validNum == -1)
					{
						timeMails.remove(tm);
					}
					else if (validNum == 0)
					{
						if (tm.canSend(curTime))
						{
							tm.setLastSendDate(curTime);
							// 发送
							// 全服发(level>=4)
							List<Integer>[] playerIds = Cache.getInstance().getAllPlayerIds();
							// 在线
							for (int playerId : playerIds[0])
							{
								Mail mail = Mail.createMail(playerId, "GM", tm.getTitle(), tm.getContent(), tm.getReward1(), tm.getReward2(), tm.getReward3(), tm.getReward4(), tm.getReward5(), tm.getReward6(), tm.getGold(), tm.getCrystal(), tm.getRuneNum(), tm.getPower(), tm.getFriendNum(), tm.getDeleteTime());
								Cache.getInstance().sendMail(mail);
							}
							// 不在线
							List<Mail> mails = new ArrayList<Mail>();
							for (int playerId : playerIds[1])
							{
								Mail mail = Mail.createMail(playerId, "GM", tm.getTitle(), tm.getContent(), tm.getReward1(), tm.getReward2(), tm.getReward3(), tm.getReward4(), tm.getReward5(), tm.getReward6(), tm.getGold(), tm.getCrystal(), tm.getRuneNum(), tm.getPower(), tm.getFriendNum(), tm.getDeleteTime());
								mails.add(mail);
							}
							Cache.getInstance().batchSaveMail(mails);
							memlogger.info("发送定时邮件完成:" + tm.getSendTime() + "|" + tm.getStartDay() + "|" + tm.getEndDay());
						}
					}
				}
				// 定时特殊条件
				for (SpeciaMail sm : speciaMails)
				{
					// 清理掉缓存中的无效特殊邮件
					int validNum = sm.valid(curTime);
					if (validNum == -1)
					{
						speciaMails.remove(sm);
					}
					if (validNum == 0)
					{
						// 发送
						// 全服发(level>=4)
						List<Integer>[] playerIds = Cache.getInstance().getAllPlayerIds();
						if (sm.getType() == 11)
						{
							
							if (sm.canSend(curTime))
							{
								sm.setLastSendDate(curTime);
								DoingPvPRankSpeciaMail(sm,playerIds);
							}
						}
						if (sm.getType() == 12)
						{
							if (sm.canDay(curTime) && sm.canSend(curTime))
							{
								sm.setLastSendDate(curTime);
								DoingBlockUnitSkillDataSpeciaMail(sm,playerIds);
							}
						}
					}
				}
				// log
				if (curTime - lastPrintLogTime >= PrintLogTime)
				{
					lastPrintLogTime = curTime;
					memlogger.info("定时邮件任务数:" + timeMails.size());
					memlogger.info("pvp排名奖励邮件任务数：" + speciaMails.size());
				}
				
				// 每分钟把现有的log保存
				List<LogBuy> logs = new ArrayList<LogBuy>();
				synchronized (lock)
				{
					logs.addAll(logBuys);
					logBuys.clear();
				}
				Cache.getInstance().saveLogbuys(logs);
				Thread.sleep(1000 * 60);
			}
			catch (Exception e)
			{
				errorlogger.error("定时发送邮件线程出错", e);
			}
		}
	}
	
	// 发活动期间pvp排行在n~n之间奖励邮件
	public void DoingPvPRankSpeciaMail(SpeciaMail sm,List<Integer>[] playerIds)
	{
		// 在线
		for (int playerId : playerIds[0])
		{
			PkRank pkRank = Cache.getInstance().getRankById(playerId);
			if (pkRank != null)
			{
				if (pkRank.getRank() >= sm.getValue() && pkRank.getRank() <= sm.getValue2())
				{
					Mail mail = Mail.createMail(playerId, "GM", sm.getTitle(), sm.getContent(), sm.getReward1(), sm.getReward2(), sm.getReward3(), sm.getReward4(), sm.getReward5(), sm.getReward6(), sm.getGold(), sm.getCrystal(), sm.getRuneNum(), sm.getPower(), sm.getFriendNum(), sm.getDeleteTime());
					Cache.getInstance().sendMail(mail);
				}
			}
		}
		// 不在线
		List<Mail> mails = new ArrayList<Mail>();
		for (int playerId : playerIds[1])
		{
			PkRank pkRank = Cache.getInstance().getRankById(playerId);
			if (pkRank != null)
			{
				if (pkRank.getRank() >= sm.getValue() && pkRank.getRank() <= sm.getValue2())
				{
					Mail mail = Mail.createMail(playerId, "GM", sm.getTitle(), sm.getContent(), sm.getReward1(), sm.getReward2(), sm.getReward3(), sm.getReward4(), sm.getReward5(), sm.getReward6(), sm.getGold(), sm.getCrystal(), sm.getRuneNum(), sm.getPower(), sm.getFriendNum(), sm.getDeleteTime());
					mails.add(mail);
				}
			}
		}
		Cache.getInstance().batchSaveMail(mails);
		memlogger.info("发送获得pvp排名" + sm.getValue() + "~" + sm.getValue2() + "之间的奖励邮件");
	}
	
	// 发活动截止时间解锁制定合体技发奖励
	public void DoingBlockUnitSkillDataSpeciaMail(SpeciaMail sm,List<Integer>[] playerIds)
	{
		// 在线
		for (int playerId : playerIds[0])
		{
			Player player = Cache.getInstance().getPlayer(playerId);
			String unitSkills = player.getUnitskills();
			if (unitSkills != null && unitSkills.length() > 0)
			{
				String[] unitSkill = unitSkills.split("&");
				for (int i = 0; i < unitSkill.length; i++)
				{
					if (sm.getValue() == Integer.parseInt(unitSkill[i]))
					{
						Mail mail = Mail.createMail(playerId, "GM", sm.getTitle(), sm.getContent(), sm.getReward1(), sm.getReward2(), sm.getReward3(), sm.getReward4(), sm.getReward5(), sm.getReward6(), sm.getGold(), sm.getCrystal(), sm.getRuneNum(), sm.getPower(), sm.getFriendNum(), sm.getDeleteTime());
						Cache.getInstance().sendMail(mail);
					}
				}
			}
		}
		// 不在线
		List<Mail> mails = new ArrayList<Mail>();
		for (int playerId : playerIds[1])
		{
			Player player = Cache.getInstance().getPlayer(playerId);
			String unitSkills = player.getUnitskills();
			if (unitSkills != null && unitSkills.length() > 0)
			{
				String[] unitSkill = unitSkills.split("&");
				for (int i = 0; i < unitSkill.length; i++)
				{
					if (sm.getValue() == Integer.parseInt(unitSkill[i]))
					{
						Mail mail = Mail.createMail(playerId, "GM", sm.getTitle(), sm.getContent(), sm.getReward1(), sm.getReward2(), sm.getReward3(), sm.getReward4(), sm.getReward5(), sm.getReward6(), sm.getGold(), sm.getCrystal(), sm.getRuneNum(), sm.getPower(), sm.getFriendNum(), sm.getDeleteTime());
						mails.add(mail);
					}
				}
			}
		}
		Cache.getInstance().batchSaveMail(mails);
	}
	
}
