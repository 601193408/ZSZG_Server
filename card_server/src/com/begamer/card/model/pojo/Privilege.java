package com.begamer.card.model.pojo;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Privilege implements Serializable{

	private int id;
	private String master;
	private int masterValue;
	private String access;
	private int accessValue;
	private int operation;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getMaster()
	{
		return master;
	}

	public void setMaster(String master)
	{
		this.master = master;
	}

	public int getMasterValue()
	{
		return masterValue;
	}

	public void setMasterValue(int masterValue)
	{
		this.masterValue = masterValue;
	}

	public String getAccess()
	{
		return access;
	}

	public void setAccess(String access)
	{
		this.access = access;
	}

	public int getAccessValue()
	{
		return accessValue;
	}

	public void setAccessValue(int accessValue)
	{
		this.accessValue = accessValue;
	}

	public int getOperation()
	{
		return operation;
	}

	public void setOperation(int operation)
	{
		this.operation = operation;
	}

}
