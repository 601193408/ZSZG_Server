package com.begamer.card.json.command2;

import com.begamer.card.json.BasicJson;

public class BuyPowerOrGoldJson extends BasicJson
{
	public int type;//1金币 2体力  3扫荡券  4推图挑战次数 5冷却时间  6pvp进入次数   7购买vip礼包    8商城或者黑市购买   9黑市位置格子购买 10血瓶购买
	public int jsonType;//1购买请求  2购买
	public int costType;//花费类型 1水晶 2金币
	public int sweepTimes;//
	public int md;//关卡id.请求购买挑战次数时用
	//购买冷却时间
	public int cdType;//冷却类型 1pk 2maze 3 event   购买请求时客户端->服务器
	public int eventId;//如果冷却时间为副本，则需副本id   购买请求时客户端->服务器
	
	//购买vip礼包
	public int giftId;//礼包id
	//商城购买
	public int shopType;//商城1或者黑市2竞技场商城3
	public int goodsId;//商城或者黑市购买id
	public int number;//商城购买的数量
	
	public int getType()
	{
		return type;
	}

	public void setType(int type)
	{
		this.type = type;
	}

	public int getJsonType()
	{
		return jsonType;
	}

	public void setJsonType(int jsonType)
	{
		this.jsonType = jsonType;
	}

	public int getCostType()
	{
		return costType;
	}

	public void setCostType(int costType)
	{
		this.costType = costType;
	}

	public int getSweepTimes()
	{
		return sweepTimes;
	}

	public void setSweepTimes(int sweepTimes)
	{
		this.sweepTimes = sweepTimes;
	}

	public int getMd()
	{
		return md;
	}

	public void setMd(int md)
	{
		this.md = md;

	}
	public int getCdType()
	{
		return cdType;
	}

	public void setCdType(int cdType)
	{
		this.cdType = cdType;
	}

	public int getEventId()
	{
		return eventId;
	}

	public void setEventId(int eventId)
	{
		this.eventId = eventId;
	}

	public int getGiftId() {
		return giftId;
	}

	public void setGiftId(int giftId) {
		this.giftId = giftId;
	}

	public int getShopType() {
		return shopType;
	}

	public void setShopType(int shopType) {
		this.shopType = shopType;
	}

	public int getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(int goodsId) {
		this.goodsId = goodsId;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}
}
