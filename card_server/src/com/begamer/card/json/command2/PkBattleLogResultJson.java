package com.begamer.card.json.command2;


import com.begamer.card.json.ErrorJson;

public class PkBattleLogResultJson extends ErrorJson
{
	/**1胜利,2失败**/
	public int r;//
	public String name;//战胜对象
	public int rank;//排名
	public int rank0;//pk前排名
	public int runeNum;//领取的每日奖励-符文值
	public int power0;//升级前体力值
	public int power1;//升级后体力值
	public int award;//每日奖励值
	public int sAward;
	public int honor;//每次奖励的荣誉值
	public int getR() {
		return r;
	}
	public void setR(int r) {
		this.r = r;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	public int getRuneNum() {
		return runeNum;
	}
	public void setRuneNum(int runeNum) {
		this.runeNum = runeNum;
	}
	public int getPower0() {
		return power0;
	}
	public void setPower0(int power0) {
		this.power0 = power0;
	}
	public int getPower1() {
		return power1;
	}
	public void setPower1(int power1) {
		this.power1 = power1;
	}
	public int getAward() {
		return award;
	}
	public void setAward(int award) {
		this.award = award;
	}
	public int getRank0() {
		return rank0;
	}
	public void setRank0(int rank0) {
		this.rank0 = rank0;
	}
	public int getSAward() {
		return sAward;
	}
	public void setSAward(int sAward) {
		this.sAward = sAward;
	}
	public int getHonor() {
		return honor;
	}
	public void setHonor(int honor) {
		this.honor = honor;
	}
}
