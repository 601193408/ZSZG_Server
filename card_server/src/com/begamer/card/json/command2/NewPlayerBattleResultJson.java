package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.CardJson;
import com.begamer.card.json.ErrorJson;

public class NewPlayerBattleResultJson extends ErrorJson
{
	public CardJson[] cs0;//cards0//
	public CardJson[] cs1;//cards1//
	public List<String> us0;//unitSkillsId0//**uniteskillId1-怒气值1,uniteskillId2-怒气值2**//
	public List<String> us1;//unitSkillsId1//
	public int[] mes;//==双方最大怒气上限==//
	public int initE;//==初始怒气==//
	
	public CardJson[] getCs0() {
		return cs0;
	}
	public void setCs0(CardJson[] cs0) {
		this.cs0 = cs0;
	}
	public CardJson[] getCs1() {
		return cs1;
	}
	public void setCs1(CardJson[] cs1) {
		this.cs1 = cs1;
	}
	public List<String> getUs0() {
		return us0;
	}
	public void setUs0(List<String> us0) {
		this.us0 = us0;
	}
	public List<String> getUs1() {
		return us1;
	}
	public void setUs1(List<String> us1) {
		this.us1 = us1;
	}
	public int[] getMes()
	{
		return mes;
	}
	public void setMes(int[] mes)
	{
		this.mes = mes;
	}
	public int getInitE()
	{
		return initE;
	}
	public void setInitE(int initE)
	{
		this.initE = initE;
	}
}
