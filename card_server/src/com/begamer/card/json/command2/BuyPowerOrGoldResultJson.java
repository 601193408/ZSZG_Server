package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;

public class BuyPowerOrGoldResultJson extends ErrorJson
{
	public int crystal;//水晶数(购买请求时表示所需水晶数，购买时表示剩余水晶数)
	public int num;//购买请求时表示购买的体力或者金币数或者挑战次数或者扫荡券的个数，购买时表示总的金币或者体力或者挑战次数或者扫荡券的个数
	public int times;//购买请求时剩余购买次数
	public int md;//关卡id     购买位置之后表示新刷出的位置的物品   id
	public List<String> ids;//可以购买的vip礼包ids
	public String goodsInfo;//购买物品的类型-物品id    goodstype-itemId
	
	public int getCrystal() {
		return crystal;
	}
	public void setCrystal(int crystal) {
		this.crystal = crystal;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public int getTimes() {
		return times;
	}
	public void setTimes(int times) {
		this.times = times;
	}
	public int getMd() {
		return md;
	}
	public void setMd(int md) {
		this.md = md;
	}
	public List<String> getIds() {
		return ids;
	}
	public void setIds(List<String> ids) {
		this.ids = ids;
	}
	public String getGoodsInfo() {
		return goodsInfo;
	}
	public void setGoodsInfo(String goodsInfo) {
		this.goodsInfo = goodsInfo;
	}
	
}
